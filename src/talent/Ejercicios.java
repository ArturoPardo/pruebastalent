package talent;

public class Ejercicios {
	
	public static void ejercicio1(int limite) {
		for (int i = 0; i <= limite; i++) {
			System.out.println(i);

		}
	}

	public static void ejercicio2(int limite) {
		int[] listadoImpares = new int[limite / 2 + limite % 2];

		for (int i = 0; i < listadoImpares.length; i++) {
			listadoImpares[i] = i * 2 + 1;

		}
		System.out.println(listadoImpares.length);
	}

	public static void ejercicio22(int limite) {
		int[] listadoPares = new int[limite / 2];

		for (int i = 0; i < listadoPares.length; i++) {
			listadoPares[i] = i * 2 + 2;
			System.out.println(listadoPares[i]);
		}
		System.out.println(listadoPares.length);

	}


	public static void ejercicio3(String cadena) {
		boolean esNumero = true;
	
		for (int i = 0; i < cadena.length(); i++) {
			char caracter = cadena.charAt(i);
			if (!Character.isDigit(caracter)) {
				esNumero = false;
				break;
			}
		}
		if (esNumero) {
			int numero = Integer.parseInt(cadena);
			System.out.println("El numero multiplicado es: " + numero * 5);
		} else if (cadena.contains("DANGER")) {
			System.out.println("Hay peligro");
		} else {
			System.out.println("No hay nada de que preocuparse");
		}

	}

	public static void ejercicio4(float cantidad, float moneda) {
		if (cantidad > 0) {
			ejercicio4(cantidad - moneda, moneda);
			System.out.println("Toma una moneda de: " + moneda);
		}
	}

}
